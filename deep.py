import numpy as np
import torch
import os

from .utils import get_h_from_av_yaw

class TrainManager():
    
    def __init__(self, model, criterion, optimizer, epochs, batch_size, gpuid=0):
        self.device = 'cuda:' + str(gpuid) if torch.cuda.is_available() else 'cpu'
        self.model = model.to(self.device)
        self.criterion = criterion
        self.optimizer = optimizer
        self.epochs = epochs
        self.batch_size = batch_size
        self.train_loss_log = []
        self.val_loss_log = []
        
    def fit(self, batch_train, batch_val, show_progress=True):
        min_val_epoch_loss = 1e5
        try:
            for epoch in range(1, self.epochs+1):
                train_epoch_loss = self.train(batch_train)
                val_epoch_loss = self.val(batch_val)
                self.train_loss_log.append(train_epoch_loss)
                self.val_loss_log.append(val_epoch_loss)
                if min_val_epoch_loss > val_epoch_loss:
                    self.best_model = self.model
                    min_val_epoch_loss = val_epoch_loss
                if show_progress:
                    print('='*30)
                    print('train: {:2.4f} | val: {:2.4f} | {}/{}'.format(train_epoch_loss, val_epoch_loss, epoch, self.epochs))
        except KeyboardInterrupt:
            print('Exiting from training early')
            
    def train(self, batch_data):
        self.model.train()
        epoch_loss = 0
        pre_seq_id = -1
        loss = 0
        iter_num = 0
        
        for seq_id, seq_feat, tar in batch_data:
            # init hidden
            if seq_id.data.item() !=pre_seq_id:
                hidden = torch.zeros(self.model.num_layers, 1, self.model.hidden_size).to(self.device)
                cell = torch.zeros(self.model.num_layers, 1, self.model.hidden_size).to(self.device)
                pre_seq_id = seq_id.data.item()
            # to cuda
            seq_feat = seq_feat.to(self.device)
            tar = tar.to(self.device)
            # Truncate hidden
            hidden = hidden.detach()
            cell = cell.detach()
            # predict
            seq_pred_grav, hidden, cell = self.model(seq_feat, hidden, cell)
            seq_pred_gyro_z = (-seq_pred_grav[0, :, :] * seq_feat[0, :, 3:]).sum(1, keepdim=True)[:, 0]
            pred_h = torch.sum(seq_pred_gyro_z)/batch_data.dataset.freq
            # loss
            loss = loss + self.criterion(pred_h, tar[0], seq_pred_grav[0, :, :], seq_feat[0, :, :3])
            iter_num += 1
            # batch
            if iter_num % self.batch_size == 0 or len(batch_data) - iter_num == 0:
                epoch_loss += loss.data.item()
                self.optimizer.zero_grad()
                loss = loss/self.batch_size
                loss.backward()
                self.optimizer.step()
                loss = 0
        return epoch_loss/len(batch_data)

    def val(self, batch_data):
        self.model.eval()
        epoch_loss = 0
        pre_seq_id = -1
        
        for seq_id, seq_feat, tar in batch_data:
            # init hidden
            if seq_id.data.item() !=pre_seq_id:
                hidden = torch.zeros(self.model.num_layers, 1, self.model.hidden_size).to(self.device)
                cell = torch.zeros(self.model.num_layers, 1, self.model.hidden_size).to(self.device)
                pre_seq_id = seq_id.data.item()
            # to cuda
            seq_feat = seq_feat.to(self.device)
            tar = tar.to(self.device)
            # Truncate hidden
            hidden = hidden.detach()
            cell = cell.detach()
            # predict
            seq_pred_grav, hidden, cell = self.model(seq_feat, hidden, cell)
            seq_pred_gyro_z = (-seq_pred_grav[0, :, :] * seq_feat[0, :, 3:]).sum(1, keepdim=True)[:, 0]
            pred_h = torch.sum(seq_pred_gyro_z)/batch_data.dataset.freq
            # loss
            epoch_loss = epoch_loss + self.criterion(pred_h, tar[0], seq_pred_grav[0, :, :], seq_feat[0, :, :3]).data.item()
        return epoch_loss/len(batch_data)
    
    def save(self, dir_path):
        if not os.path.exists(dir_path):
            os.makedirs(dir_path)
        save_path = os.path.join(dir_path, 'model.pt')
        torch.save({
            'best_model_state_dict': self.best_model.state_dict(),
            'train_loss_log': self.train_loss_log,
            'val_loss_log': self.val_loss_log
            }, save_path)


class PredictManager():
    
    def __init__(self, model, gt=False, gpuid=0):
        self.gt = gt
        self.device = 'cuda:' + str(gpuid) if torch.cuda.is_available() else 'cpu'
        self.model = model.to(self.device)
        
    def predict(self, batch_data):
        self.model.eval()
        pred_grav = []
        pred_gyro_z = []
        # init hidden
        hidden = torch.zeros(self.model.num_layers, 1, self.model.hidden_size).to(self.device)
        cell = torch.zeros(self.model.num_layers, 1, self.model.hidden_size).to(self.device)
        for seq_id, seq_feat, tar in batch_data:
            # predict
            seq_feat = seq_feat.to(self.device)
            seq_pred_grav, hidden, cell = self.model(seq_feat, hidden, cell)
            seq_pred_gyro_z = (-seq_pred_grav[0, :, :] * seq_feat[0, :, 3:]).sum(1, keepdim=True)[:, 0]
            hidden = hidden.detach()
            cell = cell.detach()
            pred_grav += seq_pred_grav[0, :, :].to('cpu').detach().numpy().tolist()
            pred_gyro_z += seq_pred_gyro_z.to('cpu').detach().numpy().tolist()
        return np.array(pred_grav), get_h_from_av_yaw(pred_gyro_z, freq=batch_data.dataset.freq)
        
    def load(self, dir_path):
        load_path = os.path.join(dir_path, 'model.pt')
        if torch.cuda.is_available():
            checkpoint = torch.load(load_path)
        else:
            checkpoint = torch.load(load_path, map_location=torch.device('cpu'))
        self.model.load_state_dict(checkpoint['best_model_state_dict'])
        self.train_loss_log = checkpoint['train_loss_log']
        self.val_loss_log = checkpoint['val_loss_log'] 