import numpy as np
import glob
import os
import torch
from joblib import Parallel, delayed

from .utils import get_v, get_av_yaw, get_h_from_av_yaw, project_gyro_in_grav

class OxiodDatasetManager():
    
    def __init__(self, imu_path, freq, init_cut=0):
        self.freq = freq
        self.init_cut = init_cut
        self.imu_path = imu_path
        if imu_path.split('/')[-4] == 'large scale':
            self.gt_path = imu_path.replace('imu', 'tango')
        else:
            self.gt_path = imu_path.replace('imu', 'vi')
        self.read_oxiod_dataset(self.imu_path, self.gt_path)
        self.v_2d, self.v_norm = get_v(self.pos, self.freq)
        self.av_yaw = get_av_yaw(self.quat, self.freq)
        self.h = get_h_from_av_yaw(self.av_yaw, self.freq)    
    
    def read_oxiod_dataset(self, imu_path, gt_path):
        imu = np.loadtxt(imu_path, delimiter=',')
        gt = np.loadtxt(gt_path, delimiter=',')  # large scaleではimu.csvとtango.csvでdata_lengthが合わない
        gt = gt[:len(imu), :]

        self.ts = np.array([i*0.01 for i in range(len(imu))])[self.init_cut:]
        self.gyro = imu[self.init_cut:, 4:7]
        self.grav = imu[self.init_cut:, 7:10]
        self.linacc = imu[self.init_cut:, 10:13]
        self.mag = imu[self.init_cut:, 13:16]
        self.pos = gt[self.init_cut:, 2:5] 
        self.quat = gt[self.init_cut:, 5:9][:, [3, 0, 1, 2]]
        self.acc = self.linacc + self.grav


class SequenceDataset(torch.utils.data.Dataset):
    
    def __init__(self, DatasetManager, data_path_list, window, slide, init_cut=0, freq=200, gt=True, n_jobs=20):
        self.DatasetManager = DatasetManager
        self.freq = freq
        self.init_cut = init_cut
        self.gt = gt
        self.ts_list, self.acc_list, self.linacc_list, self.grav_list, self.gyro_list, self.v_norm_list, self.av_yaw_list = [], [], [], [], [], [], []
        data_object_list = self.multi_load_data_object(data_path_list, n_jobs=n_jobs)
        for dm in data_object_list:
            self.ts_list.append(dm.ts)
            self.acc_list.append(dm.acc)
            self.gyro_list.append(dm.gyro)
            self.v_norm_list.append(dm.v_norm)
            self.av_yaw_list.append(dm.av_yaw)
                
        self.window = window
        self.slide = slide
        self.index_map = []
        for i in range(len(self.ts_list)):
            ts = self.ts_list[i]
            for j in range(0, len(ts)-self.window, self.slide):
                self.index_map.append([i, j])
            
    def __len__(self):
        return len(self.index_map)
    
    def __getitem__(self, idx):
        seq_id, frame_id = self.index_map[idx][0], self.index_map[idx][1]
        feature = np.concatenate([
            self.acc_list[seq_id][frame_id:frame_id+self.window], 
            self.gyro_list[seq_id][frame_id:frame_id+self.window]], axis=1
        )
            
        if self.gt:
            target = np.sum(self.av_yaw_list[seq_id][frame_id:frame_id+self.window])/self.freq
            return seq_id, feature.astype(np.float32), target.astype(np.float32)
        else:
            return feature.astype(np.float32)
        
    def load_data_object(self, data_path):
        dm = self.DatasetManager(data_path, freq=self.freq, init_cut=self.init_cut)
        return dm
    
    def multi_load_data_object(self, data_path_list, n_jobs):
        data_object_list = Parallel(n_jobs=n_jobs)([delayed(self.load_data_object)(data_path) for data_path in data_path_list])
        return data_object_list


def get_train_val_list_oxiod(dataset_dir):

    def get_data_list(dataset_dir, data_type):
        data_list = []
        txt_path_list = glob.glob(os.path.join(dataset_dir, '**', data_type), recursive=True)
        for txt_path in txt_path_list:
            with open(txt_path, 'r') as f:
                tmp = f.read().split('\n')
            for x in tmp:
                data_list.append(txt_path.replace(data_type, x).replace('imu', 'syn/imu'))
        return data_list

    data_type = 'Train.txt'
    train_list = get_data_list(dataset_dir, data_type)
    data_type = 'Test.txt'
    val_list = get_data_list(dataset_dir, data_type)
    return train_list, val_list