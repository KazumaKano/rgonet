# README #

RGONet: robust gravity and orientation estimation neural network

# Usage
## Install
```
cd  rgonet
pip install -r requirements.txt
```

## Train
Set config in train.py
```
config = {
    'dataset': 'oxiod',
    'window': 1600,
    'batch_size': 2048,
    'lr': 1e-3,
    'epochs': 1,
    'init_cut': 0,
    'freq': 100,
    'k': 1,
    'hp': {
        'hidden_size': 64,
        'num_layers': 1,
        'dropout': 0.25,
    }
}
```

Execute following script
```
python train.py --gpuid [gpuid] --dataset [dataset path] --result_dir [result_dir path]
```

## Predict & show result
Execute show_result.ipynb

## Evaluate
Set test data list in evaluate.py
```
test_path_list = ['large scale/floor1/syn/imu1.csv', ..., 'large scale/floor4/syn/imu18.csv']
```

Execute following script
```
python evaluate.py --gpuid [gpuid] --dataset [dataset path] --result_dir [result_dir]
```

