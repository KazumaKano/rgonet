import numpy as np
import torch
import os
from sklearn.metrics import mean_squared_error
import argparse
from tqdm import tqdm

from data import OxiodDatasetManager, SequenceDataset
from model import LSTM
from utils import save_json, read_json, project_gyro_in_grav, get_h_from_av_yaw, get_grav_by_lowpass, ekf
from deep import PredictManager

class EvaluateManager():
    
    def __init__(self, DatasetManager, result_dir, eval_method, window):
        self.DatasetManager = DatasetManager
        self.result_dir = result_dir
        self.window = window
        self.eval_method = eval_method

    def evaluate_test_data(self, test_path):
        # load config
        config = read_json(os.path.join(self.result_dir, 'config.json'))
        # load data
        dm = self.DatasetManager(test_path, freq=config['freq'])
        test_seq_dataset = SequenceDataset(self.DatasetManager, [test_path], 
                                        window=self.window,
                                        slide=self.window,
                                        init_cut=config['init_cut'],
                                        freq=config['freq'],
                                        gt=True,
                                        n_jobs=20)
        batch_test = torch.utils.data.DataLoader(test_seq_dataset, batch_size=1, shuffle=False, num_workers=4)
        # prepare for predict
        model = LSTM(input_size=6, output_size=3, hp=config['hp'])
        pm = PredictManager(model, gt=True, gpuid=0)
        pm.load(os.path.join(self.result_dir))
        # predict
        pred_grav, pred_h = pm.predict(batch_test)
        ## gravity
        grav_api = dm.grav
        grav_lp = get_grav_by_lowpass(dm.acc)
        grav_ekf = ekf(dm.gyro, grav_lp, dm.grav[0])
        grav_gnet = pred_grav
        ## heading
        h_api = get_h_from_av_yaw(project_gyro_in_grav(dm.gyro, -grav_api), freq=config['freq'])
        h_lp = get_h_from_av_yaw(project_gyro_in_grav(dm.gyro, -grav_lp), freq=config['freq'])
        h_ekf = get_h_from_av_yaw(project_gyro_in_grav(dm.gyro, -grav_ekf), freq=config['freq'])
        h_gnet = pred_h
        # calculate mse score
        if self.eval_method == 'mse':
            score_api = mean_squared_error(h_api, dm.h)
            score_lp = mean_squared_error(h_lp, dm.h)
            score_ekf = mean_squared_error(h_ekf, dm.h)
            score_gnet = mean_squared_error(h_gnet, dm.h[:len(h_gnet)])
            return [score_api, score_lp, score_ekf, score_gnet]
        elif self.eval_method == 'err_per_sec':
            score_api = self.eval_err_per_sec(h_api, dm.h, freq=config['freq'])
            score_lp = self.eval_err_per_sec(h_lp, dm.h, freq=config['freq'])
            score_ekf = self.eval_err_per_sec(h_ekf, dm.h, freq=config['freq'])
            score_gnet = self.eval_err_per_sec(h_gnet, dm.h[:len(h_gnet)], freq=config['freq'])
            return [score_api, score_lp, score_ekf, score_gnet]

    def evaluate_test_list(self, test_path_list):
        score_api_list = []
        score_lp_list = []
        score_ekf_list = []
        score_gnet_list = []
        
        for test_path in tqdm(test_path_list):
            score_list = self.evaluate_test_data(test_path)
            score_api_list.append(score_list[0])
            score_lp_list.append(score_list[1])
            score_ekf_list.append(score_list[2])
            score_gnet_list.append(score_list[3])
        return score_api_list, score_lp_list, score_ekf_list, score_gnet_list
    
    def eval_err_per_sec(self, pred_h, gt_h, freq):
        err, cnt = 0, 0
        sec = 20
        slide_sec = 2
        segment = freq*sec
        for i in range(0, len(pred_h)-segment, slide_sec):
            err += np.abs((pred_h[i+segment]-pred_h[i])-(gt_h[i+segment]-gt_h[i]))
            cnt += sec
        return err/cnt

def main(test_path_list, args):
    # calculate mean score
    eval_result_dict = {}

    # MSE Loss
    eval_method = 'mse'
    print(eval_method)
    eval_result_dict[eval_method] = {}
    em = EvaluateManager(OxiodDatasetManager, args.result_dir, eval_method, args.window)
    score_api_list, score_lp_list, score_ekf_list, score_gnet_list = em.evaluate_test_list(test_path_list)
    eval_result_dict[eval_method]['api'] = np.mean(score_api_list)
    eval_result_dict[eval_method]['lowpass'] = np.mean(score_lp_list)
    eval_result_dict[eval_method]['ekf'] = np.mean(score_ekf_list)
    eval_result_dict[eval_method]['rgonet'] = np.mean(score_gnet_list)

    # Err per sec
    eval_method = 'err_per_sec'
    print(eval_method)
    eval_result_dict[eval_method] = {}
    em = EvaluateManager(OxiodDatasetManager, args.result_dir, eval_method, args.window)
    score_api_list, score_lp_list, score_ekf_list, score_gnet_list = em.evaluate_test_list(test_path_list)
    eval_result_dict[eval_method]['api'] = np.mean(score_api_list)
    eval_result_dict[eval_method]['lowpass'] = np.mean(score_lp_list)
    eval_result_dict[eval_method]['ekf'] = np.mean(score_ekf_list)
    eval_result_dict[eval_method]['rgonet'] = np.mean(score_gnet_list)

    # save eval result
    save_json(os.path.join(args.result_dir, 'eval.json'), eval_result_dict)

    # show result
    for eval_method, eval_result in eval_result_dict.items():
        print('='*30)
        print(eval_method)
        for key, value in eval_result.items():
            print('{}: {:2.4f}'.format(key, value))


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--gpuid', type=int, default=0)
    parser.add_argument('--dataset', type=str)
    parser.add_argument('--result_dir', type=str, default='test')
    parser.add_argument('--window', type=int, default=100)
    args = parser.parse_args()

    test_path_list = [
        'large scale/floor1/syn/imu1.csv',
        'large scale/floor1/syn/imu3.csv',
        'large scale/floor1/syn/imu4.csv',
        'large scale/floor1/syn/imu5.csv',
        'large scale/floor1/syn/imu6.csv',
        'large scale/floor1/syn/imu7.csv',
        'large scale/floor1/syn/imu8.csv',
        'large scale/floor1/syn/imu10.csv',
        'large scale/floor4/syn/imu2.csv',
        'large scale/floor4/syn/imu3.csv',
        'large scale/floor4/syn/imu4.csv',
        'large scale/floor4/syn/imu5.csv',
        'large scale/floor4/syn/imu8.csv',
        'large scale/floor4/syn/imu10.csv',
        'large scale/floor4/syn/imu11.csv',
        'large scale/floor4/syn/imu12.csv',
        'large scale/floor4/syn/imu14.csv',
        'large scale/floor4/syn/imu15.csv',
        'large scale/floor4/syn/imu17.csv',
        'large scale/floor4/syn/imu18.csv']

    test_path_list = [os.path.join(args.dataset, test_path) for test_path in test_path_list]

    main(test_path_list, args)