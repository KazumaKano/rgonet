import numpy as np
import os
import json
import quaternion
import copy

def save_json(path, data):
    save_path = os.path.join(path)
    with open(save_path, 'w') as f:
        json.dump(data, f, indent=2, ensure_ascii=False)

def read_json(path):
    path = os.path.join(path)
    with open(path, 'r') as f:
        data = json.load(f)
    return data

def get_v(pos, freq):
    v_2d = np.zeros_like(pos[:, 0:2])
    v_2d[:-1, 0] = np.diff(pos[:, 0])
    v_2d[:-1, 1] = np.diff(pos[:, 1])
    v_2d[-1, 0] = v_2d[-2, 0]
    v_2d[-1, 1] = v_2d[-2, 1]
    v_norm = np.linalg.norm(v_2d, axis=1)*freq
    return v_2d, v_norm

def get_av_yaw(quat, freq, spike_thr=3):
    av_yaw = np.zeros(len(quat))
    q = quaternion.as_quat_array(quat)
    euler_angles = quaternion.as_euler_angles(q)
    r = euler_angles[:, 0]
    av_yaw[0] = 0
    av_yaw[1:] = remove_spike(np.diff(r), spike_thr)*freq
    return av_yaw

def remove_spike(data, spike_thr):
    rs_data = copy.deepcopy(data)
    for i in range(len(data)):
        if np.abs(data[i]) > spike_thr:
            rs_data[i] = 0
    return rs_data

def get_h_from_av_yaw(av_yaw, freq=100):
    return np.cumsum(av_yaw)/freq

def get_trajectory(l, h, h0=0, freq=100):
    l = l/freq
    x = np.cumsum(l*np.cos(h+h0))
    y = np.cumsum(l*np.sin(h+h0))
    return x, y

def project_gyro_in_grav(seq_gyro, seq_grav):
    gyro_g = np.array([np.dot(gyro, grav)/np.linalg.norm(grav) for gyro, grav in zip(seq_gyro, seq_grav)])
    return gyro_g

def low_pass_filter(f, fs, fc=1, dt=0.01):
    N = len(f)
    fc_upper = fs - fc
    freq = np.linspace(0, 1.0/dt, N)
    F = np.fft.fft(f)
    G = F.copy()
    G[((freq > fc)&(freq< fc_upper))] = 0 + 0j
    g = np.fft.ifft(G)
    return g.real

def get_grav_by_lowpass(acc):
    grav_x = low_pass_filter(acc[:, 0], fs=100)
    grav_y = low_pass_filter(acc[:, 1], fs=100)
    grav_z = low_pass_filter(acc[:, 2], fs=100)   
    return np.concatenate([grav_x[:, None], grav_y[:, None], grav_z[:, None]], axis=1)

def ekf(w_list, r_m_list, r_s, sigma_init=1, sigma_g=1, sigma_a=9, dt=0.01):
    def get_skew_symmetric_matrix(vec3d):
        x, y, z = vec3d[0,0], vec3d[1,0], vec3d[2,0]
        ssm = np.array([
            [0, -z, y],
            [z, 0, -x],
            [-y, x, 0]
        ])
        return ssm

    def get_transformation_matrix(q):
        q_v = q[0:3]
        q_c = q[3, 0]
        I = np.identity(3)
        q_v_ssm = get_skew_symmetric_matrix(q_v)
        C = (q_c**2 - (q_v.T @ q_v)[0, 0]) * I - 2 * q_c * q_v_ssm + 2 * q_v @ q_v.T
        return C

    def convert_row_to_col_vec(row_vec):
        col_vec = row_vec[:, None]
        return col_vec

    def initialization(sigma_init):
        q = np.array([0, 0, 0, 1])[:, None]
        I = np.identity(3)
        P = sigma_init**2 * I
        return q, P

    def propagation_step(q, P, w, sigma_g, dt=0.005):
        beta = get_beta(w, dt)
        omega = get_omega(w)
        F = get_F(w, beta, omega)
        update_q = F @ q
        Phi = get_Phi(w, dt)
        Q = get_Q(sigma_g, dt)
        update_P = Phi @ P @ Phi.T + Q
        return update_q, update_P 

    def get_beta(w, dt):
        beta = np.linalg.norm(w) * dt / 2
        return beta

    def get_omega(w):
        w_ssm = get_skew_symmetric_matrix(w)
        w_x, w_y, w_z = w[0, 0], w[1, 0], w[2, 0]
        omega = np.array([
            [-w_ssm[0, 0], -w_ssm[0, 1], -w_ssm[0, 2], w_x],
            [-w_ssm[1, 0], -w_ssm[1, 1], -w_ssm[1, 2], w_y],
            [-w_ssm[2, 0], -w_ssm[2, 1], -w_ssm[2, 2], w_z],
            [-w_x, -w_y, -w_z, 0]
        ])
        return omega

    def get_F(w, beta, omega):
        I = np.identity(4)
        F = I*np.cos(beta) + omega * np.sin(beta) / np.linalg.norm(w)
        return F

    def get_Phi(w, dt):
        I = np.identity(3)
        w_ssm = get_skew_symmetric_matrix(w)
        Phi = I - w_ssm * dt
        return Phi

    def get_Q(sigma_g, dt):
        I = np.identity(3)
        Q = sigma_g**2 * dt * I
        return Q

    def mesurement_update_step(q, P, r_m, r_s, sigma_a=9):
        I = np.identity(3)
        C = get_transformation_matrix(q)
        r = C @ r_s
        H = get_skew_symmetric_matrix(r)
        R = get_R(sigma_a)
        K = P @ H.T @ np.linalg.inv(H@P@H.T + R)
        d_theta = K @ (r_m - r)
        S = get_S(q)
        a = S @ d_theta
        update_q = q + S @ d_theta / 2
        update_P = (I - K @ H) @ P @ (I - K @ H).T + K @ R @ K.T
        return update_q, update_P

    def get_R(sigma_a):
        I = np.identity(3)
        Q = sigma_a**2 * I
        return Q

    def get_S(q):
        q_v, q_c = q[0:3], q[3, 0]
        I = np.identity(3)
        q_v_ssm = get_skew_symmetric_matrix(q_v)
        s = q_c * I + q_v_ssm
        S = np.array([
            [s[0, 0], s[0, 1], s[0, 2]],
            [s[1, 0], s[1, 1], s[1, 2]],
            [s[2, 0], s[2, 1], s[2, 2]],
            [-q_v[0, 0], -q_v[1, 0], -q_v[2, 0]]
        ])
        return S
    
    r_list = []
    q, P = initialization(sigma_init)
    r_s = np.array(r_s)[:, None]
    for w_, r_m_ in zip(w_list, r_m_list):
        w, r_m = w_[:, None], r_m_[:, None]
        q, P = propagation_step(q, P, w, sigma_g, dt=0.005)
        q, P = mesurement_update_step(q, P, r_m, r_s, sigma_a=9)
        C = get_transformation_matrix(q)
        r = C @ r_s
        r_list.append(r)
    return np.array(r_list)[:, :, 0]